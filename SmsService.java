package com.wavelabs.ai;

import java.util.concurrent.CompletableFuture;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * 
 * @author gopi
 *
 */
@Service
public class SmsService {

	@Async
	public void sendMessage(Job job) {
		System.out.println("Sending message started");
		try {
			Thread.sleep(10 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Sending message completed");
	}

	@Async
	public CompletableFuture<Job> sendMessageIn(Job job) {
		System.out.println("Processing started for thread: " + Thread.currentThread().getId());
		try {
			Thread.sleep(5 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}PUT
		System.out.println("Processing completed for thread: " + Thread.currentThread().getId());
		return CompletableFuture.completedFuture(job);
	}

}
