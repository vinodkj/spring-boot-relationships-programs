package ai.wavelabs.eureka;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class SampleController {

	@Autowired
	RestTemplate restTemplate;

	@RequestMapping("/test")
	public ResponseEntity<Map> getMap() {
		ResponseEntity<Map> response = restTemplate.getForEntity("http://SCHOOL/port", Map.class);
		return response;
	}
}
