package com.wavelabs.ai;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SuffixValidator implements ConstraintValidator<Suffix, String> {

	private String prefix;

	@Override
	public void initialize(Suffix constraintAnnotation) {
		this.prefix = constraintAnnotation.prefix();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {

		if (value.startsWith(prefix)) {
			return true;
		}
		return false;
	}

}
