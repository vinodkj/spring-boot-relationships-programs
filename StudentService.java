package com.wavelabs.ai;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

/**
 * 
 * @author gopi
 *
 */
@Service
public class StudentService {

	@Autowired
	private StudentRepository studentRepository;

	public Student saveStudent(Student student) {
		return studentRepository.save(student);
	}

	private void printTransactionInfo() {
		if (TransactionAspectSupport.currentTransactionStatus().isNewTransaction()) {
			System.out.println("new transaction");
		} else {
			System.out.println("old transaction");
		}
	}

	@Cacheable(cacheNames = "students", key = "#id")
	public Student getStudent(Integer id) {
		return studentRepository.findById(id).get();
	}

	@CachePut(cacheNames = "students", key = "#id")
	public Student updateStudent(Student student, Integer id) {
		student.setId(id);
		Student student2 = studentRepository.saveAndFlush(student);
		return student2;
	}

	@CacheEvict(key = "#id", cacheNames = "students")
	public void deleteStudent(Integer id) {
		studentRepository.deleteById(id);
	}

}
