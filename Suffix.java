package com.wavelabs.ai;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ FIELD })
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = { SuffixValidator.class })
public @interface Suffix {

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	String prefix() default "WL";
	
	String message() default "Please give correct prefix";

}
